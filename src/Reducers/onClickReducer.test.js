import { onClickAction } from './../Actions/onClickAction';
import onClickReducer from './onClickReducer';

describe('On Click Reducer', () => {

    it('Should return default state', () => {
        const newState = onClickReducer(undefined, {});
        expect(newState).toEqual({
            Score: 0,
            Wickets: 0,
            Balls: 0
        });
    });

    it('Should return new state if receiving type', () => {

        const score = {"Balls": 1, "Score": 2, "Wickets": 3};
        const newState = onClickReducer(undefined, {
            type: 'ON_CLICK_ACTION',
            payload: score
        });
        expect(newState).toEqual(score);

    });

});