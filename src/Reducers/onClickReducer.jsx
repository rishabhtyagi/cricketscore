const DefaultCricketScore = {
    Score: 0,
    Wickets: 0,
    Balls: 0
}
export default (state = DefaultCricketScore, action) => {
 switch (action.type) {
  case 'ON_CLICK_ACTION':
   return  {
    ...state,
    Score: action.payload.Score,
    Wickets: action.payload.Wickets,
    Balls: action.payload.Balls 
   };
  default:
   return state
 }
}