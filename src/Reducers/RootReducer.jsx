import { combineReducers } from 'redux';
import onClickReducer from './onClickReducer';
export default combineReducers({
    onClickReducer
});