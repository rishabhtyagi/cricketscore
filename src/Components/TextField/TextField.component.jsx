import React from 'react';
import PropTypes from 'prop-types';
import './TextField.styles.css';

const TextField = props => (
<label data-test="textFieldComponent" id={props.id}>{props.attribute} :  {props.value}</label>
);

TextField.propTypes = {
    attribute: PropTypes.string,
    value: PropTypes.number
}

export default TextField;