import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TextField from './TextField.component'
import { findByTestAtrr, checkProps } from './../../Utils'

configure({ adapter: new Adapter() });

const setUp = (props = {}) => {
    const component = shallow(<TextField {...props} />);
    return component;
};

describe('Text Field Component', () => {

    let component;
    beforeEach(() => {
        component = setUp();
    });

    it('Should render without errors', () => {
        const wrapper = findByTestAtrr(component, 'textFieldComponent');
        expect(wrapper.length).toBe(1);
    });

    describe('Checking PropTypes', () => {

        it('Should not throw a warning', () => {

            const expectedProps = {
                attribute: 'Score',
                value: 2
            };
            const propsErr = checkProps(TextField, expectedProps)
            expect(propsErr).toBeUndefined();

        });

    });

})