import React from 'react';
import './Main.styles.css';
import TextField from '../TextField/TextField.component';
import Button from '../Button/Button.component';
import { onClickAction } from '../../Actions/onClickAction';
import { connect } from 'react-redux';

class Main extends React.Component {

    onButtonClick = event => {
        if (event) {
            switch (event.target.id) {
                case "AddBall":
                    this.props.onClickReducer.Balls++;
                    break;
                case "One":
                    this.props.onClickReducer.Score++;
                    break;
                case "Four":
                    this.props.onClickReducer.Score += 4;
                    break;
                case "Wicket":
                    this.props.onClickReducer.Wickets++;
                    break;
                default:

            }
        }
        this.props.onClickAction(this.props.onClickReducer);
    }

    render() {
        return (
            <div className='App' id="Container">
                <h1>Score Board</h1>
                <TextField attribute="Score" value={this.props.onClickReducer.Score}></TextField>
                <TextField attribute="Wicket" value={this.props.onClickReducer.Wickets}></TextField>
                <TextField attribute="Balls" value={this.props.onClickReducer.Balls}></TextField>
                <div>
                    <Button text="AddBall" onButtonClick={this.onButtonClick} ></Button>
                    <Button text="One" onButtonClick={this.onButtonClick}></Button>
                    <Button text="Four" onButtonClick={this.onButtonClick}></Button>
                    <Button text="Wicket" onButtonClick={this.onButtonClick}></Button>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    ...state
})

const mapDispatchToProps = dispatch => ({
    onClickAction: (onClick) => dispatch(onClickAction(onClick))
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);