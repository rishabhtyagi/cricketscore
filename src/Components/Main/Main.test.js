import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import Main from './Main.component'
const mockStore = configureStore([]);

describe('My Connected React-Redux Main Component', () => {
    let store;
    let component;
    beforeEach(() => {
        
        store = mockStore({
            onClickReducer: {
                Score: 2,
                Wickets: 2,
                Balls: 4
            },
        });
        store.dispatch = jest.fn();
        component = renderer.create(
            <Provider store={store}>
              <Main />
            </Provider>
          );

    });

    it('should render with given state from Redux store', () => {
        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should dispatch an action on button click', () => {
        renderer.act(() => {
           component.toJSON().children[4].children[0].props.onClick();
           expect(store.dispatch).toHaveBeenCalledTimes(1);
          });

    });

});