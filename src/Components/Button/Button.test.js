import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button from './Button.component'
import { findByTestAtrr, checkProps } from '../../Utils'

configure({ adapter: new Adapter() });

const setUp = (props = {}) => {
    const component = shallow(<Button {...props} />);
    return component;
};

describe('Button Component', () => {

    let component;
    let mockFunc;
    beforeEach(() => {
        mockFunc = jest.fn();
        const Props = {
            text: 'Submit',
            onButtonClick: mockFunc
        }
        component = setUp(Props);
    });

    it('Should render button without errors', () => {
        const wrapper = findByTestAtrr(component, 'buttonComponent');
        expect(wrapper.length).toBe(1);
    });

    describe('Checking PropTypes', () => {

        it('Should not throw a warning', () => {

            const expectedProps = {
                text: 'Submit',
                onButtonClick: () => {

                }
            };
            const propsErr = checkProps(Button, expectedProps)
            expect(propsErr).toBeUndefined();

        });

    });

    it('Should emit callback on click event', () => {
        const button = findByTestAtrr(component, 'buttonComponent');
        button.simulate('click');
        const callback = mockFunc.mock.calls.length;
        expect(callback).toBe(1);
    });

})