import React from 'react';
import PropTypes from 'prop-types';
import './Button.styles.css';

const Button = props => (
<button data-test="buttonComponent" id={props.text} onClick={props.onButtonClick}>{props.text}</button>
);

Button.propTypes = {
    text: PropTypes.string,
    emitEvent: PropTypes.func
}

export default Button